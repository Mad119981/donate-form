const Router = require('@koa/router');

const router = new Router();

const donate = require('./donate');

router.use('/donate', donate.routes());

module.exports = router;
