const Router = require('@koa/router');

const router = new Router();
const donate = require('../controllers/donate');

router.post('/', donate.donate);

module.exports = router;
