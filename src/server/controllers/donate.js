const Donate = require('../lib/db/schemas/Donate');
const currencies = require('../../lib/currencies.json');

const currencyCodes = currencies.map((currency) => currency.code);

module.exports = {
  donate: async (ctx) => {
    const { amount, currency } = ctx.request.body;

    if (typeof amount !== 'number' || isNaN(amount) || amount <= 0) {
      return ctx.throw(400, `Invalid 'amount': ${amount}`);
    }

    if (typeof currency !== 'string' || currencyCodes.indexOf(currency) === -1) {
      return ctx.throw(400, `Invalid 'currency': ${currency}`);
    }

    try {
      await Donate.create({ amount, currency });
    } catch (err) {
      return ctx.throw(err);
    }

    ctx.body = { ok: true };
  },
};
