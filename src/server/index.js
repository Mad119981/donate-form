require('dotenv').config();

const Koa = require('koa');
const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser');
const serve = require('koa-static');
const path = require('path');

const app = new Koa();
const logger = require('koa-logger');
const db = require('./lib/db');
const router = require('./routes');

const connection = db.connect();

// logger
app.use(logger());

// error handling
app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || 500;
    ctx.body = err.message;
    ctx.app.emit('error', err, ctx);
  }
});

app.on('error', (err, ctx) => {
  if (ctx.status >= 500) {
    console.error(err);
  }
});

app.use(cors());
app.use(bodyParser());

app.use(serve(path.resolve(__dirname, '../../dist')));

app.use(async (ctx, next) => {
  await connection;
  await next();
});

// x-response-time
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', `${ms}ms`);
});

// router
app.use(router.routes());

const port = +process.env.API_PORT || 3000;
app.listen(port, () => console.log(`server listening on port: ${port}`));
