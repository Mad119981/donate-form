const mongoose = require('mongoose');

const DonateSchema = new mongoose.Schema(
  {
    amount: {
      type: Number,
      required: true,
      min: 1,
    },
    currency: {
      type: String,
      required: true,
      minlength: 1,
    },
  },
  {
    collation: 'donates',
    minimize: false,
    versionKey: false,
    timestamps: {
      createdAt: 'created',
      updatedAt: -1,
    },
  },
);

module.exports = mongoose.model('Donate', DonateSchema);
