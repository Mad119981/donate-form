const mongoose = require('mongoose');

const options = {
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true,
  useUnifiedTopology: true,
  replicaSet: process.env.DB_REPLICA_SET,
};

if (process.env.DB_USERNAME) {
  options.authSource = 'admin';
}

const protocol = process.env.DB_SRV ? 'mongodb+srv' : 'mongodb';
const port = process.env.DB_SRV ? '' : `:${process.env.DB_PORT || 27017}`;
const uri = process.env.DB_USERNAME
  ? `${protocol}://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}${port}/${process.env.DB_NAME}`
  : `${protocol}://${process.env.DB_HOST}${port}/${process.env.DB_NAME}`;

async function connect() {
  mongoose.connection.once('open', () => {
    console.log(`connected to database: ${mongoose.connection.name} ${mongoose.connection.host}:${mongoose.connection.port}`);
  });

  await mongoose.connect(uri, options);

  return mongoose.connection;
}

module.exports = {
  connect,
};
