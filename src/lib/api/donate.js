import api from './instance';

export default {
  async send({ amount, currency }) {
    if (typeof amount !== 'number' || isNaN(amount) || amount < 1) {
      throw new Error(`Invalid amount: ${amount}`);
    }

    if (typeof currency !== 'string' || currency.length === 0) {
      throw new Error(`Invalid currency: ${currency}`);
    }

    await api.post('/donate', { amount, currency });
  },
};
