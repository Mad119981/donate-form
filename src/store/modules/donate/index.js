import donate from '@/lib/api/donate';

export default {
  namespaced: true,
  actions: {
    async sendDonate(store, { amount, currency }) {
      return donate.send({ amount, currency });
    },
  },
};
