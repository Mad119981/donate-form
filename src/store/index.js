import Vue from 'vue';
import Vuex from 'vuex';
import donate from './modules/donate';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {

  },
  modules: {
    donate,
  },
});
