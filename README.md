# donate-form :gift:

## Project setup :wrench:
Install packages  
```
yarn install
```

Set environment variables with `.env` file or otherwise  

Environment variables:  
```dotenv
# SERVER
# server port
API_PORT=3000

# dtabase settings
# keep DB_REPLICA_SET or DB_SRV variables empty if don't want to use that
DB_HOST=localhost
DB_PORT=27017
DB_NAME=Donate
DB_REPLICA_SET=
DB_SRV=

# CLIENT
# base path ro REST api
VUE_APP_API_HOST=http://localhost:3000
```

:rocket:  
Build frontend app with `yarn build`  
Start server with `yarn api`  

:tada:
